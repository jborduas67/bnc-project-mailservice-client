FROM maven:3.6.0-jdk-8-alpine as builder
WORKDIR /tmp
COPY src/ src/
COPY pom.xml .
RUN mvn clean package

FROM openjdk:8-jdk-alpine
EXPOSE 8080
ARG JAR_FILE=/tmp/target/bnc-project-mailservice-client-1.0.0-SNAPSHOT.jar
COPY --from=builder ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]